package ru.agaldkov.kotlin_vk_sdk_template.presenters

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.agaldkov.kotlin_vk_sdk_template.R
import ru.agaldkov.kotlin_vk_sdk_template.models.FriendModel
import ru.agaldkov.kotlin_vk_sdk_template.providers.FriendsProvider
import ru.agaldkov.kotlin_vk_sdk_template.views.FriendsView
import ru.agaldkov.kotlin_vk_sdk_template.views.LoginView

/**
 * Created by sunwi on 09.11.2017.
 */

@InjectViewState
class FriendsPresenter: MvpPresenter<FriendsView>() {
    fun loadFriends() {
        viewState.startLoading()
        FriendsProvider(presenter = this).loadFriends()
    }

    fun friendsLoaded(friendsList: ArrayList<FriendModel>) {
        viewState.endLoading()
        if (friendsList.size == 0) {
            viewState.setupEmptyList()
            viewState.showError(textResource = R.string.friends_no_items)
        } else {
            viewState.setupFriendsList(friendsList = friendsList)
        }
    }

    fun showError(textResource: Int) {
        viewState.showError(textResource = textResource)
    }
}