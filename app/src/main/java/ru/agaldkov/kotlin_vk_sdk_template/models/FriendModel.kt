package ru.agaldkov.kotlin_vk_sdk_template.models

/**
 * Created by sunwi on 09.11.2017.
 */
class FriendModel(var _name: String, var surname: String, var city: String?,
                  var avatar: String?, var isOnline: Boolean) {
}